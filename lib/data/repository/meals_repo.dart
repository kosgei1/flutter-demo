import 'package:sendy_eats/data/models/meals.dart';
import 'package:sendy_eats/data/remote/meals_api_client.dart';

class MealsRepo {
  final MealsApiClient mealsApiClient;

  MealsRepo(this.mealsApiClient) : assert(mealsApiClient != null);

  Future<Meals> getMeals() async {
    return mealsApiClient.getMeals();
  }
}
