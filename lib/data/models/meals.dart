class Meals {
  List<Meal> meals;

  Meals({this.meals});

  Meals.fromJson(Map<String, dynamic> json) {
    if (json['meals'] != null) {
      meals = <Meal>[];
      json['meals'].forEach((v) {
        meals.add(Meal.fromJson(v));
      });
    }
  }
}

class Meal {
  String strMeal, strMealThumb, idMeal;

  Meal({this.strMeal, this.strMealThumb, this.idMeal});

  Meal.fromJson(Map<String, dynamic> json) {
    strMeal = json['strMeal'];
    strMealThumb = json['strMealThumb'];
    idMeal = json['idMeal'];
  }
}
