import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:sendy_eats/data/models/meals.dart';

class MealsApiClient {
  static const url =
      "https://www.themealdb.com/api/json/v1/1/filter.php?c=Pork";

  final http.Client client;

  MealsApiClient(this.client) : assert(client != null);

  Future<Meals> getMeals() async {
    try {
      final mealsResponse = await client.get(Uri.parse(url));

      if (mealsResponse.statusCode != 200) {
        throw Exception('Something went thing wrong');
      }
      return Meals.fromJson(jsonDecode(mealsResponse.body));
    } catch (e) {
      throw Exception('Something went thing wrong');
    }
  }
}
