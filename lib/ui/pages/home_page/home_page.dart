import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:sendy_eats/bloc/meals_bloc/meals_bloc.dart';
import 'package:sendy_eats/data/remote/meals_api_client.dart';
import 'package:sendy_eats/data/repository/meals_repo.dart';
import 'package:http/http.dart' as http;

import 'widgets/meals_items.dart';

class HomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.green,
        title: Text(
          "Sendy Eats".toUpperCase(),
          style: TextStyle(
            fontWeight: FontWeight.bold,
          ),
        ),
      ),
      body: BlocProvider(
        create: (context) => MealBloc(MealsRepo(MealsApiClient(http.Client())))
          ..add(FetchMeals()),
        child: BlocBuilder<MealBloc, MealsState>(
          builder: (context, state) {
            if (state is MealsLoaded) {
              return ListView.builder(
                  itemCount: state.meals.length,
                  itemBuilder: (context, index) {
                    return MealsItem(
                      meal: state.meals[index],
                      index: index,
                    );
                  });
            } else if (state is MealsError) {
              return Center(
                child: Text(state.error),
              );
            } else {
              return CircularProgressIndicator();
            }
          },
        ),
      ),
    );
  }
}
