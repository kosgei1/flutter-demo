import 'package:flutter/material.dart';
import 'package:sendy_eats/data/models/meals.dart';

class MealsItem extends StatelessWidget {
  final Meal meal;
  final int index;

  const MealsItem({Key key, this.meal, this.index}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Container(
      height: size.height / 2,
      decoration: BoxDecoration(
          image: DecorationImage(
              image: NetworkImage(meal.strMealThumb), fit: BoxFit.cover)),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment:
            index.isEven ? CrossAxisAlignment.end : CrossAxisAlignment.start,
        children: [
          Container(
            color: Colors.green,
            width: size.width / 2,
            padding: EdgeInsets.all(8.0),
            child: Text(
              meal.strMeal.toUpperCase(),
              style: TextStyle(
                color: Colors.white,
                fontSize: 20,
                fontWeight: FontWeight.bold,
              ),
            ),
          )
        ],
      ),
    );
  }
}
