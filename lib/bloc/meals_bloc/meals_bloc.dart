import 'package:equatable/equatable.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:sendy_eats/data/models/meals.dart';
import 'package:sendy_eats/data/repository/meals_repo.dart';

part 'meals_state.dart';
part 'meals_event.dart';

class MealBloc extends Bloc<MealsEvent, MealsState> {
  final MealsRepo mealsRepo;
  MealBloc(this.mealsRepo) : super(MealsIntial());

  @override
  Stream<MealsState> mapEventToState(MealsEvent event) async* {
    if (event is FetchMeals) {
      yield MealsLoading();
      try {
        Meals meals = await mealsRepo.getMeals();
        yield MealsLoaded(meals.meals);
      } catch (e) {
        yield MealsError("$e");
      }
    }
  }
}
