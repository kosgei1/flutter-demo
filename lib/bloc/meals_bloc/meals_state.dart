part of 'meals_bloc.dart';

abstract class MealsState extends Equatable {
  const MealsState();

  @override
  List<Object> get props => [];
}

class MealsIntial extends MealsState {}

class MealsLoading extends MealsState {}

class MealsError extends MealsState {
  final String error;
  MealsError(this.error);
}

class MealsLoaded extends MealsState {
  final List<Meal> meals;

  MealsLoaded(this.meals);
}
